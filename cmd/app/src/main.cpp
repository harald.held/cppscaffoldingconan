#include "logger.h"

constexpr auto loggerName = "pn";

int main()
{
    auto log = logging::logger(loggerName);
    log->info("in main()");

    return 0;
}