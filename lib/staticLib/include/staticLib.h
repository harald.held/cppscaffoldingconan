#pragma once

#include <memory>

namespace spdlog
{
class logger;
}

class StaticLib
{
public:
    StaticLib();

private:
    std::shared_ptr<spdlog::logger> log_;
};
