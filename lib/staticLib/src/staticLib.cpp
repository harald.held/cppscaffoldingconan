#include "staticLib.h"
#include "logger.h"

namespace
{
constexpr auto loggerName = "pn-static-lib";
}

StaticLib::StaticLib()
    : log_(logging::logger(loggerName))
{
    log_->info("constructed StaticLib object");
}
