#pragma once

#include "sharedlib_export.h"

#include <memory>

namespace spdlog
{
class logger;
}

class SharedLib
{
public:
    PREFIX_SHAREDLIB_EXPORT SharedLib();

private:
    std::shared_ptr<spdlog::logger> log_;
};
