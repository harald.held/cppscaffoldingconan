#include "sharedLib.h"
#include "logger.h"

namespace
{
constexpr auto loggerName = "pn-shared-lib";
}

SharedLib::SharedLib()
    : log_(logging::logger(loggerName))
{
    log_->info("constructed SharedLib object");
}
