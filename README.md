# Install or update dependencies

```shell
conan install . -c tools.cmake.cmaketoolchain:generator=Ninja -of build/conan --build=missing -s build_type=Debug -pr:b=default
conan install . -c tools.cmake.cmaketoolchain:generator=Ninja -of build/conan --build=missing -s build_type=Release -pr:b=default
```